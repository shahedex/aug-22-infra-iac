resource "aws_instance" "web" {
  ami           = "ami-082b1f4237bd816a1"
  instance_type = "t2.micro"
  key_name = "ec2-key"
  subnet_id = aws_subnet.public_subnet.id
  associate_public_ip_address = true
  vpc_security_group_ids = [aws_security_group.allow_all.id]
  tags = {
    Name = "webserver"
  }
}
