resource "aws_vpc" "aug_vpc" {
  cidr_block       = "10.0.0.0/16"

  tags = {
    Name = "aug_vpc_22"
  }
}

resource "aws_subnet" "public_subnet" {
  vpc_id     = aws_vpc.aug_vpc.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "public-subnet"
  }
}

resource "aws_subnet" "private_subnet" {
  vpc_id     = aws_vpc.aug_vpc.id
  cidr_block = "10.0.2.0/24"

  tags = {
    Name = "private-subnet"
  }
}
