terraform {
  backend "s3" {
    bucket = "aug-22-terraform-state-bucket"
    key    = "iac/terraform.tfstate"
    region = "ap-southeast-1"
  }
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.60.0"
    }
    tls = {
      source = "hashicorp/tls"
      version = "4.0.4"
    }
  }
}

provider "aws" {
  region = "ap-southeast-1"
}

provider "tls" {
}
